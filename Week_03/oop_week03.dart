
void main(List<String> args) {
  Persegi persegi = new Persegi();

  persegi.luas(10);
  persegi.keliling(12);

}

class Persegi {
  void luas(s) {
    int hasil = s * s;
    return print('luas lingkaran ${hasil}');
  }

  void keliling(s) {
    int hasil = s * 4;
    return print('Keliling lingkaran ${hasil}');
  }
}
