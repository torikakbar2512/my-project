void main(List<String> args) {
  // Hay();

  // hello("Tharixs");

  // penjumlahan(8,10);

  // tampilkanNilai(12);

  undifined();
}

// function sederhana
void Hay() {
  print("Hello World");
}

// function dengan parameter
void hello(String nama) {
  return print(nama);
}

// funtion dengan parameter lebih dari satu
void penjumlahan(int a, int b) {
  return print(a + b);
}

// funtion dengan nilai default
void tampilkanNilai(n1, {dn: 20}) {
  print(n1);
  print(dn);
}

// funtion UNDEFINED
undifined() {
  print("this is undefined function");
}
