void main(List<String> args) {
  // parent class
  // Titan titan = new Titan();
  // titan.setPowerPoin(90);

  // child class
  // armor_titan armorTitan = new armor_titan();
  // armorTitan.setPowerPoin(2000);
  // armorTitan.terjang();

  // attack_titan attTitan = new attack_titan();
  // attTitan.setPowerPoin(2000);
  // attTitan.punch();

  // beast_titan beastTitan = new beast_titan();
  // beastTitan.setPowerPoin(1000);
  // beastTitan.lempar();

  // human Human = new human();
  // Human.killAllTitan();
}

class Titan {
  int powerPoin = 0;
  setPowerPoin(p) {
    if (p <= 5) {
      powerPoin = 5;
    } else {
      powerPoin = p;
    }
    print(powerPoin);
  }

  getPowerPoin(g) {
    int value = g;
    return print(value);
  }
}

class armor_titan extends Titan {
  @override
  setPowerPoin(p) {
    // TODO: implement setPowerPoin
    return super.setPowerPoin(p);
  }

  @override
  getPowerPoin(g) {
    // TODO: implement getPowerPoin
    return super.getPowerPoin(g);
  }

  void terjang() {
    print("wush wush");
  }
}

class attack_titan extends Titan {
  @override
  setPowerPoin(p) {
    // TODO: implement setPowerPoin
    return super.setPowerPoin(p);
  }

  @override
  getPowerPoin(g) {
    // TODO: implement getPowerPoin
    return super.getPowerPoin(g);
  }

  void punch() {
    print("blum - blum ");
  }
}

class beast_titan extends Titan {
  @override
  setPowerPoin(p) {
    // TODO: implement setPowerPoin
    return super.setPowerPoin(p);
  }

  @override
  getPowerPoin(g) {
    // TODO: implement getPowerPoin
    return super.getPowerPoin(g);
  }

  void lempar() {
    print("wush wush");
  }
}

class human {
  void killAllTitan() {
    print("Sasageo ... Sasageo");
  }
}
