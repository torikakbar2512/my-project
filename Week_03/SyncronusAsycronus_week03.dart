// syncronus

// void main(List<String> args) {
//   print("nama saya tharixs ");
//   Future.delayed(Duration(seconds: 5), () {
//     print("Umur saya 19");
//   });

//   print("saya berasal dari jember");
// }

// syncronus akan menjalankan program berikutnya sembari menunggu program future selesai

// Asyncronus

void main(List<String> args) async {
  print("nama saya tharixs ");
  await Future.delayed(Duration(seconds: 5), () {
    print("Umur saya 19");
  });

  print("saya berasal dari jember");  
}

// asyncronus berjalan beriringan 