
void main(List<String> args) {
  Employee(12, "Tharixs", "UniDev");
}

class Employee {
  late int id;
  late String name ;
  late String department ;

  Employee(id, name, department) {
    this.id = id;
    this.name = name;
    this.department = department;
    print(
        'id : ${this.id}\n name : ${this.name}\n department : ${this.department}');
  }
}
