import 'package:app/content/Navigation.dart';
import 'package:app/content/Route.dart';
import 'package:app/content/RowAndColum.dart';
import 'package:flutter/material.dart';

// void main() {
//   runApp(MaterialApp(
//     debugShowCheckedModeBanner: false,
//     onGenerateRoute: RouteGenerator.generateRoute,
//   ));
// }

// // // Row And Colum
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RowAndColum();
  }
}
