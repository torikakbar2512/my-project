import 'package:flutter/material.dart';

void main(List<String> args) {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: "Belajar Form Flutter",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  const BelajarForm({Key? key}) : super(key: key);

  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();
  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Belajar form flutter"),
        ),
        body: Form(
            key: _formKey,
            child: SingleChildScrollView(
                child: Container(
              padding: EdgeInsets.all(20.0),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: new InputDecoration(
                        hintText: "contoh : Tharixs Akbar Ibnu Azis",
                        labelText: "Nama Lengkap",
                        icon: Icon(Icons.people),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Nama tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      obscureText: true,
                      decoration: new InputDecoration(
                        labelText: "Passwoard",
                        icon: Icon(Icons.security),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Password tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                  ),
                  CheckboxListTile(
                      title: Text('Belajar pemrograman'),
                      subtitle: Text('Dart , Widget, http'),
                      value: nilaiCheckBox,
                      onChanged: (value) {
                        setState(() {
                          nilaiCheckBox = value!;
                        });
                      }),
                  SwitchListTile(
                      title: Text('Backand Programing'),
                      subtitle: Text('Dart, NodeJs, PHP, JAVA, dll '),
                      value: nilaiSwitch,
                      activeTrackColor: Colors.pink[100],
                      activeColor: Colors.red,
                      onChanged: (value) {
                        setState(() {
                          nilaiSwitch = value;
                        });
                      }),
                  Slider(
                    value: nilaiSlider,
                    min: 0,
                    max: 100,
                    onChanged: (value) {
                      setState(() {
                        nilaiSlider = value;
                      });
                    },
                  ),
                  RaisedButton(
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Colors.blue,
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {}
                    },
                  )
                ],
              ),
            ))));
  }
}
