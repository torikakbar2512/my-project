import 'dart:html';

import 'package:flutter/material.dart';

void main(List<String> args) {
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    home: new Home(),
  ));
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<String> agama = [
    "Islam",
    "Kristen Protestan",
    "Kristen Katolik",
    "Hindu",
    "Budha",
    "Konghucu"
  ];
  String _agama = "Islam";
  String _jk = "";

  TextEditingController controllerNama = new TextEditingController();
  TextEditingController controllerPass = new TextEditingController();
  TextEditingController controllerMoto = new TextEditingController();

  void _pilihJk(String value) {
    setState(() {
      _jk = value;
    });
  }

  void pilihanAgama(String value) {
    setState(() {
      _agama = value;
    });
  }

  void kirimData() {
    AlertDialog alertDialog = new AlertDialog(
      content: new Container(
        height: 200.0,
        child: new Column(
          children: <Widget>[
            new Text("Nama Lengkap : ${controllerNama.text}"),
            new Text("Password: ${controllerPass.text}"),
            new Text("Moto Hidup : ${controllerMoto.text}"),
            new Text("Jenis Kelamin : ${_jk}"),
            new Text("Moto Hidup : ${_agama}"),
            new RaisedButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.pop(context);
              },
              color: Colors.teal,
            )
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        leading: new Icon(Icons.list),
        title: new Text("Data diri"),
        backgroundColor: Colors.teal,
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          children: [
            new Container(
              padding: new EdgeInsets.all(10.0),
              child: new Column(
                children: <Widget>[
                  new TextFormField(
                    controller: controllerNama,
                    decoration: new InputDecoration(
                        hintText: "Name Lengkap",
                        labelText: "Name Lengkap",
                        border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(20.0))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Nama Lengkap tidak boleh kosong";
                      }
                      return null;
                    },
                  ),
                  Padding(
                    padding: new EdgeInsets.only(top: 20.0),
                    child: TextFormField(
                      controller: controllerPass,
                      obscureText: true,
                      decoration: new InputDecoration(
                          hintText: "Passwoard",
                          labelText: "Passwoard",
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20.0))),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Password tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: new EdgeInsets.only(top: 20.0),
                    child: TextFormField(
                      controller: controllerMoto,
                      maxLines: 3,
                      decoration: new InputDecoration(
                          hintText: "Moto Hidup",
                          labelText: "Moto Hidup",
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20.0))),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Moto tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                  ),
                  new Padding(
                    padding: new EdgeInsets.only(top: 20.0),
                  ),
                  new RadioListTile(
                    value: "laki laki",
                    title: new Text("laki laki"),
                    groupValue: _jk,
                    onChanged: (String? value) {
                      _pilihJk(value!);
                    },
                    activeColor: Colors.blue,
                    subtitle: new Text("Pilih ini jika anda laki laki"),
                  ),
                  new RadioListTile(
                    value: "perempuan",
                    title: new Text("Perempuan"),
                    groupValue: _jk,
                    onChanged: (String? value) {
                      _pilihJk(value!);
                    },
                    activeColor: Colors.blue,
                    subtitle: new Text("Pilihan ini jika anda perempuan "),
                  ),
                  new Padding(
                    padding: new EdgeInsets.only(top: 20.0),
                  ),
                  new Row(
                    children: <Widget>[
                      new Text(
                        "Agama ",
                        style:
                            new TextStyle(fontSize: 18.0, color: Colors.blue),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(left: 15.0),
                      ),
                      DropdownButton(
                        onChanged: (String? value) {
                          pilihanAgama(value!);
                        },
                        value: _agama,
                        items: agama.map((String value) {
                          return new DropdownMenuItem(
                            value: value,
                            child: new Text(value),
                          );
                        }).toList(),
                      ),
                    ],
                  ),
                  new RaisedButton(
                      child: new Text("Ok"),
                      color: Colors.blue,
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          kirimData();
                        }
                      })
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
