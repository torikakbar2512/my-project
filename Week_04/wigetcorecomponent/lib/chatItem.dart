import 'package:flutter/material.dart';

class chatItem extends StatelessWidget {
  const chatItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 10),
          width: 60,
          height: 60,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: AssetImage('assets/logo.png'),
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Tharixs',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 6,
              ),
              Text('Bagaimana kabar kamu?'),
            ],
          ),
        ),
        Text('Now'),
      ],
    );
  }
}
