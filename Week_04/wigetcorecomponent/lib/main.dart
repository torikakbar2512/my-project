import 'package:flutter/material.dart';
import 'package:wigetcorecomponent/chatItem.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('My App'),
//         backgroundColor: Colors.redAccent,
//       ),
//       body: Column(
//         children: <Widget>[
//           Container(
//             color: Colors.redAccent,
//             height: 100,
//             width: 100,
//           ),
//           Container(
//             color: Colors.blueAccent,
//             height: 100,
//             width: 100,
//           ),
//           Container(
//             color: Colors.greenAccent,
//             height: 100,
//             width: 100,
//           ),
//           Container(
//             color: Colors.yellowAccent,
//             height: 100,
//             width: 100,
//           ),
//         ],
//       ),
//     );
//   }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //login page design

      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/login.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Login",
                style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: 300,
                child: TextField(
                  decoration: InputDecoration(
                    hintText: "Enter your email",
                    hintStyle: TextStyle(
                      color: Colors.grey,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: BorderSide(
                        color: Colors.grey,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: BorderSide(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: 300,
                child: TextField(
                  decoration: InputDecoration(
                    hintText: "Enter your password",
                    hintStyle: TextStyle(
                      color: Colors.grey,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: BorderSide(
                        color: Colors.grey,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: BorderSide(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: 300,
                child: RaisedButton(
                  onPressed: () {},
                  color: Colors.white,
                  child: Text(
                    "Login",
                    style: TextStyle(
                      color: Colors.blueAccent,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: 300,
                child: RaisedButton(
                  onPressed: () {},
                  color: Colors.white,
                  child: Text(
                    "Sign Up",
                    style: TextStyle(
                      color: Colors.blueAccent,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }


//   @override
//   Widget build(BuildContext context) {
    
//     return Scaffold(
// // drawer
//       drawer: Drawer(
//         child: ListView(
//           children: <Widget>[
//             UserAccountsDrawerHeader(
//               accountName: Text("Tharixs Akbar"),
//               accountEmail: Text("Tharixs@gmail.com"),
//               currentAccountPicture: CircleAvatar(
//                 backgroundColor: Colors.white,
//                 child: Text("TA"),
//               ),
//             ),
//             ListTile(
//               title: Text("Home"),
//               leading: Icon(Icons.home),
//               onTap: () {
//                 Navigator.pop(context);
//               },
//             ),
//             ListTile(
//               title: Text("My Account"),
//               leading: Icon(Icons.person),
//               onTap: () {
//                 Navigator.pop(context);
//               },
//             ),
//             ListTile(
//               title: Text("My Orders"),
//               leading: Icon(Icons.shopping_basket),
//               onTap: () {
//                 Navigator.pop(context);
//               },
//             ),
//             ListTile(
//               title: Text("Categories"),
//               leading: Icon(Icons.dashboard),
//               onTap: () {
//                 Navigator.pop(context);
//               },
//             ),
//             ListTile(
//               title: Text("Favourites"),
//               leading: Icon(Icons.favorite),
//               onTap: () {
//                 Navigator.pop(context);
//               },
//             ),
//             Divider(),
//             ListTile(
//               title: Text("Settings"),
//               leading: Icon(Icons.settings),
//               onTap: () {
//                 Navigator.pop(context);
//               },
//             ),
//             ListTile(
//               title: Text("About"),
//               leading: Icon(Icons.help),
//               onTap: () {
//                 Navigator.pop(context);
//               },
//             ),
//           ],
//         ),
//       ),
//       appBar: AppBar(
//         title: Text("Telegram"),
//         actions: <Widget>[
//           IconButton(
//             icon: Icon(Icons.search),
//             onPressed: () {},
//           ),
//         ],
//       ),
//       body: ListView(
//         padding: EdgeInsets.only(top: 30, left: 24, right: 24),
//         children: [
//           chatItem(),
//           chatItem(),
//           chatItem(),
//           chatItem(),
//           chatItem(),
//           chatItem(),
//           chatItem(),
//           chatItem(),
//         ],
//       ),

//     );
// }
}