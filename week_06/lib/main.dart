import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

void main(List<String> args) {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: new Home(),
  ));
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final List<String> gambar = [
    'bt1.gif',
    'bt2.gif',
    'bt21.gif',
    'bts.gif',
    'chimmy.gif',
    'koala.gif',
    'love.gif',
    'van.gif'
  ];

  static const Map<String, Color> colors = {
    'bt1': Color(0xFF3366FF),
    'bt2': Color(0xFF00CCFF),
    'bt21': Color(0xFF3366FF),
    'bts': Color(0xFF00CCFF),
    'chimmy': Color(0xFF3366FF),
    'koala': Color(0xFF00CCFF),
    'love': Color(0xFF3366FF),
    'van': Color(0xFF00CCFF),
  };
  @override
  Widget build(BuildContext context) {
    timeDilation = 5.8;
    return Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
            gradient: new LinearGradient(
          begin: FractionalOffset.topCenter,
          end: FractionalOffset.bottomCenter,
          colors: [
            const Color(0xFF3366FF),
            const Color(0xFF00CCFF),
          ],
        )),
        child: new PageView.builder(
            controller: new PageController(viewportFraction: 0.8),
            itemCount: gambar.length,
            itemBuilder: (BuildContext context, int i) {
              return new Padding(
                padding:
                    new EdgeInsets.symmetric(horizontal: 5.0, vertical: 50.0),
                child: new Material(
                  elevation: 8.0,
                  child: new Stack(
                    fit: StackFit.expand,
                    children: [
                      new Hero(
                          tag: gambar[i],
                          child: new Material(
                            child: new InkWell(
                              child: new Flexible(
                                flex: 1,
                                child: Container(
                                  color: colors.values.elementAt(i),
                                  child: new Image.asset(
                                    "img/${gambar[i]}",
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              onTap: () => Navigator.of(context).push(
                                  new MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          new Halamandua(
                                            gambar: gambar[i],
                                            colors: colors.values.elementAt(i),
                                          ))),
                            ),
                          ))
                    ],
                  ),
                ),
              );
            }),
      ),
    );
  }
}

class Halamandua extends StatefulWidget {
  Halamandua({required this.gambar, required this.colors});
  final String gambar;
  final Color colors;

  @override
  State<Halamandua> createState() => _HalamanduaState();
}

class Pilihan {
  const Pilihan({required this.teks, required this.warna});
  final String teks;
  final Color warna;
}

List<Pilihan> listPilihan = [
  Pilihan(teks: 'BTS', warna: Color(0xFF3366FF)),
  Pilihan(teks: 'BT21', warna: Color(0xFF00CCFF)),
  Pilihan(teks: 'Chimmy', warna: Color(0xFF3366FF)),
  Pilihan(teks: 'Koala', warna: Color(0xFF00CCFF)),
  Pilihan(teks: 'Love', warna: Color(0xFF3366FF)),
  Pilihan(teks: 'Van', warna: Color(0xFF00CCFF)),
];

class _HalamanduaState extends State<Halamandua> {
  Color warna = Colors.white;

  void _pilih(Pilihan pilihan) {
    setState(() {
      warna = pilihan.warna;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: widget.colors,
        title: new Text("BT21"),
        actions: //popup
            [
          new PopupMenuButton<Pilihan>(
            onSelected: _pilih,
            itemBuilder: (BuildContext context) {
              return listPilihan.map((Pilihan pilihan) {
                return new PopupMenuItem<Pilihan>(
                  child: new Text(pilihan.teks),
                  value: pilihan,
                );
              }).toList();
            },
          )
        ],
      ),
      body: new Stack(
        children: [
          new Container(
              decoration: new BoxDecoration(
            gradient: new RadialGradient(
              center: const Alignment(-0.25, -0.25),
              radius: 0.5,
              colors: [
                const Color(0xFF3366FF),
                const Color(0xFF00CCFF),
              ],
            ),
          )),
          new Center(
            child: new Hero(
                tag: widget.gambar,
                child: new ClipOval(
                  child: new SizedBox(
                    width: 200.0,
                    height: 200.0,
                    child: new Material(
                      child: new InkWell(
                        onTap: () => Navigator.of(context).pop(),
                        child: new Flexible(
                          flex: 1,
                          child: Container(
                            color: widget.colors,
                            child: new Image.asset(
                              "img/${widget.gambar}",
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                )),
          )
        ],
      ),
    );
  }
}
