import 'dart:convert';
import 'package:http/http.dart' show Client, Response;
import '';
import 'package:minggu10_movie/model/popular_movies.dart';

class ApiProvider {
  String apiKey = 'e691dabc90bd75d596c74e75dbaa47f3';
  String baseUrl = 'https://api.themoviedb.org/3';

  Client client = Client();

  Future<PopularMovies> getPopularMovies() async {
    Response response =
        await client.get('$baseUrl/movie/popular?api_key=$apiKey');
    if (response.statusCode == 200) {
      return PopularMovies.fromJson(jsonDecode(response.body));
    } else {
      throw Exception(response.statusCode);
    }
  }
}
